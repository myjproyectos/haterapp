$(document).ready(function(){
    var paramstr = window.location.search.substr(1);
    var paramarr = paramstr.split ("&");
    var params = {};

    for ( var i = 0; i < paramarr.length; i++) {
        var tmparr = paramarr[i].split("=");
        params[tmparr[0]] = tmparr[1];
    }
    
    var loading = `<div class="d-flex justify-content-center position-absolute loadingCenter">
                        <div class="spinner-grow" role="status"></div>
                   </div>`;

    var data = {
        title : params['title'],
        desc: params['desc'],
        url: params['url']
    } 
    $.ajax({
        data: data,
        type: 'post',
        url: 'https://myjalbion.000webhostapp.com/haterApp/peticiones/phates.php',
        before: function(){
            $(".hateContainer").html(loading)
        },
        success: function(r){
            $(".hateContainer").html(r)
            activateFunctions()
        }
    })

    var data = {
        email: !localStorage.getItem("usuario") ? '' : localStorage.getItem("usuario"),
        noticia: decodeURI(getVariable("url")),
        index: false,
        reaccion: false,
        tipo: false
    }
    
    ajax(data);

    function activateFunctions(){

        if (localStorage.getItem("usuario")) {
            $("#registro").hide()
        }
       
        $(".comentar").on('click', function () {
            $("#odiar").val('')
            $("#responder, #reaccion").hide()
            if (localStorage.getItem("usuario")) {
                $("#hate").show()
            }
            $(".f-b ").slideDown()
        })

        $("#close").on('click', function () {
            $(".f-b ").slideToggle()
        })

        $(".registro").on('click', function () {
            var email = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);
            var valor = $("#register").val();
            var pass = btoa($("#register_pass").val())
            if (valor != '' && email.test(valor)) {
                localStorage.setItem("usuario", valor)
                localStorage.setItem("pass", pass)
                var data = {
                    email: valor,
                    pass: pass,
                    tipo: 'registro',
                    noticia: $("#noticia").val(),
                    index: false,
                    reaccion: false
                }
                ajax(data)
                $("#registro").hide()
            } else {
                $("#register").addClass('alert-danger')
            }
        })

        $(".envio").on('click', function () {
            var data = {
                'hate': $("#odiar").val(),
                'tipo': 'articulo',
                'noticia': $("#noticia").val(),
                'email': localStorage.getItem('usuario'),
                index: false,
                reaccion: false
            }
            ajax(data)
            $('html').animate({ scrollTop: $('html').prop("scrollHeight")}, 1000);
        })

        $(".respondo").on('click', function () {
            var data = {
                'resp': $("#resp").val(),
                'tipo': 'respuesta',
                'index': $(this).attr("aria-label"),
                'noticia': $("#noticia").val(),
                'email': localStorage.getItem('usuario'),
                reaccion: false
            }
            ajax(data)
        })

        $(".react").on('click', function () {
            var temp = $(this).attr("aria-label").includes("-") ? 'reaccion2' : 'reaccion1'
            var data = {
                'reaccion': $(this).attr("id"),
                'tipo': temp,
                'index': $(this).attr("aria-label"),
                'noticia': $("#noticia").val(),
                'email': localStorage.getItem('usuario')
            }
            ajax(data)
        })  

    }

    function ajax(d) {
        $(".f-b ").slideUp()
        $.ajax({
            data: d,
            type: 'post',
            url: 'https://myjalbion.000webhostapp.com/haterApp/peticiones/pfiles.php',
            before: function () {
                $(".container_info").html(loading)
            },
            success: function (r) {
                $(".container_info").html(r)
                clicks()
            }
        })
    }

    function clicks() {

        $(".reply").on('click', function () {
            $(".respondo").attr("aria-label", $(this).attr("aria-label"))
            $("#resp").val('')
            $("#hate, #reaccion").hide()
            $("#responder").show()
            $(".f-b ").slideDown()
        })

        $(".reaction").on('click', function () {
            $(".react").attr("aria-label", $(this).attr("aria-label"))
            $("#hate, #responder").hide()
            $("#reaccion").show()
            $(".f-b ").slideDown()
        })

        $(".reaction2").on('click', function () {
            var temp = $(this).parents(".padre").find(".reaction").attr("aria-label") + "-" + $(this).attr("aria-label")
            $(".react").attr("aria-label", temp)
            $("#hate, #responder").hide()
            $("#reaccion").show()
            $(".f-b ").slideDown()
        })
    }

    function getVariable(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {
                return pair[1];
            }
        }
        return false;
    }

})
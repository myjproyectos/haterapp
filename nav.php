<nav class="navbar navbar-expand-lg navbar-dark bg-dark pb-2 pt-2 fixed-top">
  <div class="container-fluid">
    <a class="navbar-brand" href="./">
        <img src="assets/angry.svg" alt="Icon app" width="35px"> <span style="text-transform: none;">HaterApp</span>
    </a>
    <button class="navbar-toggler" type="button" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <!-- <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="./">Política</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Farándula</a>
        </li> -->
        <li class="nav-item">
            <a class="nav-link" href="https://www.paypal.com/donate?hosted_button_id=5E44Q4UU8TFTY" target="_blank">Invitar un café</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
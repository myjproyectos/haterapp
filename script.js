$(document).ready(function () {
 
    $('div').each(function () {
        if ($(this).css("z-index") == 9999999) {
            $(this).hide();
        }
    })

    $(".navbar-toggler").on('click', function () {
        $(".navbar-collapse").slideToggle()
    })

    var data = {
        // email: localStorage.getItem("usuario"),
        noticia: getVariable("url"),
        index: false,
        reaccion: false,
        tipo: false
    }
    userExist()
    ajax(data);
    clicks();

    $(".registro").on('click', function () {
        var email = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);
        var valor = $("#register").val();
        var pass = $("#register_pass").val()
        if (valor != '' && email.test(valor)) {
            // localStorage.setItem("usuario", valor)
            var data = {
                email: valor,
                pass: pass,
                tipo: 'registro',
                noticia: $("#noticia").val(),
                index: false,
                reaccion: false
            }
            ajax(data)
            // userExist()
        } else {
            $("#register").addClass('alert-danger')
        }
        // $(".f-b ").slideDown()
    })

    $(".envio").on('click', function () {
        var data = {
            'hate': $("#odiar").val(),
            'tipo': 'articulo',
            'noticia': $("#noticia").val(),
            'email': localStorage.getItem('usuario'),
            index: false,
            reaccion: false
        }
        ajax(data)
    })

    $(".respondo").on('click', function () {
        var data = {
            'resp': $("#resp").val(),
            'tipo': 'respuesta',
            'index': $(this).attr("aria-label"),
            'noticia': $("#noticia").val(),
            'email': localStorage.getItem('usuario'),
            reaccion: false
        }
        ajax(data)
    })

    $(".react").on('click', function () {
        var temp = $(this).attr("aria-label").includes("-") ? 'reaccion2' : 'reaccion1'
        var data = {
            'reaccion': $(this).attr("id"),
            'tipo': temp,
            'index': $(this).attr("aria-label"),
            'noticia': $("#noticia").val(),
            'email': localStorage.getItem('usuario')
        }
        ajax(data)
    })

    $(".comentar").on('click', function () {
        $("#odiar").val('')
        // $("#responder, #reaccion").hide()
        // if (localStorage.getItem("usuario")) {
        //     $("#hate").show()
        // }
        $(".f-b ").slideDown()
    })

    function clicks() {

        $(".reply").on('click', function () {
            $(".respondo").attr("aria-label", $(this).attr("aria-label"))
            $("#resp").val('')
            $("#hate, #reaccion").hide()
            $("#responder").show()
            $(".f-b ").slideDown()
        })

        $(".reaction").on('click', function () {
            $(".react").attr("aria-label", $(this).attr("aria-label"))
            $("#hate, #responder").hide()
            $("#reaccion").show()
            $(".f-b ").slideDown()
        })

        $(".reaction2").on('click', function () {
            var temp = $(this).parents(".padre").find(".reaction").attr("aria-label") + "-" + $(this).attr("aria-label")
            $(".react").attr("aria-label", temp)
            $("#hate, #responder").hide()
            $("#reaccion").show()
            $(".f-b ").slideDown()
        })
    }

    $("#close").on('click', function () {
        $(".f-b ").slideToggle()
    })

    function ajax(d) {
        $(".f-b ").slideUp()
        $.ajax({
            data: d,
            type: 'post',
            url: 'files.php',
            before: function () {
                $(".container_info").html('<div class="spinner-grow" role="status"><span class="visually-hidden">Loading...</span></div>')
            },
            success: function (r) {
                // window.location.reload()
                $(".container_info").html(r)
                clicks()
            }
        })
    }

    function getVariable(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {
                return pair[1];
            }
        }
        return false;
    }
    
    
    function userExist() {
        // if (localStorage.getItem("usuario")) {
        //     $("#hate").show()
        //     $("#registro").hide()
        // }
        // $.ajax({
        //     data: d,
        //     type: 'post',
        //     url: 'query.php',
        //     success: function (r) {
        //         // window.location.reload()
        //         $(".container_info").html(r)
        //         clicks()
        //     }
        // })
    }

})

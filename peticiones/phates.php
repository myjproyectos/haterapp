<?php

    header('Access-Control-Allow-Origin: *');
    // session_start();

    $title = urldecode($_POST['title']);
    $desc = urldecode($_POST['desc']);
    $url = urldecode($_POST['url']);
    
    //Reacciones: Buena tigre - Me cagas - Jodete - Imbécil

?>
    <div class="container mt-5 pt-4">
        <h5><?php echo $title; ?></h5>
        <p class="text-justify"><?php echo $desc; ?></p>
        <a href="<?php echo $url; ?>" target="_blank" class="btn btn-secondary">Leer noticia completa  <i class="fas fa-chevron-right"></i></a>
    </div>
    <div class="container mt-3">
        <h5>Hates</h5>
        <div class="container_info position-relative">

        </div>
        <button class="btn btn-primary comentar mt-2 fixed-bottom" style="width: 100%">Comentar</button>
    </div>
    <div class="fixed-bottom f-b bg-warning text-white pt-3 pb-2" style="display: none">
        <i class="fa fa-angle-down position-absolute-center text-dark" id="close" role="button"></i>
        <div class="container">
                <div id="registro">             
                    <label for="register" class="form-label">Registrate</label>
                    <input type="email" id="register" class="form-control" placeholder="example@example.com">
                    <input type="password" id="register_pass" class="form-control mt-1" placeholder="Password">
                    <button type="button" style="width: 100%; padding-top: 10px; padding-bottom: 10px;" class="btn btn-danger mt-2 registro">Registrarme</button>   
                </div>
                <div id="hate" style="display: none;">
                    <input type="text" id="odiar" class="form-control" placeholder='Deja tu "hate"'>
                    <input type="hidden" hidden="hidden" id="noticia" value="<?php echo $url; ?>">
                    <button type="button" style="width: 100%; padding-top: 10px; padding-bottom: 10px;" class="btn btn-dark mt-2 envio">Enviar</button>             
                </div>
                <div id="responder" style="display: none;">
                    <input type="text" id="resp" class="form-control" placeholder="Responder">
                    <button type="button" style="width: 100%;, padding-top: 10px; padding-bottom: 10px;" class="btn btn-primary mt-2 respondo">Responder</button>
                </div>
                <div id="reaccion" style="display: none">
                    <div class="row">
                        <div class="col text-center" style="line-height: 1.2; font-weight: bold;">
                            <img width="65%" class="react" id="rA" role="button" src="img/rock.svg" alt="Así se habla">
                            <span class="lh-sm">Así se habla</span>
                        </div>
                        <div class="col text-center" style="line-height: 1.2; font-weight: bold;">
                            <img width="65%" class="react" id="rB" role="button" src="img/poop.svg" alt="Me cagas">
                            <span class="lh-sm">Me cagas</span>
                        </div>
                        <div class="col text-center" style="line-height: 1.2; font-weight: bold;">
                            <img width="65%" class="react" id="rC" role="button" src="img/stupid.svg" alt="Imbécil">
                            <span class="lh-sm">Que es imbécil</span>
                        </div>
                        <div class="col text-center" style="line-height: 1.2; font-weight: bold;">
                            <img width="65%" class="react" id="rD" role="button" src="img/jodase.svg" alt="Jodase">
                            <span class="lh-sm">Pues Jodase</span>
                        </div>
                    </div>
                </div>
        </div>
    </div>

<?php 

    header('Access-Control-Allow-Origin: *');

    function feed($feedURL){
        $i = 0; 
        $url = $feedURL; 
        $rss = simplexml_load_file($url); 
        foreach($rss->channel->item as $item) { 
            $link = $item->link;  //extrae el link
            $title = $item->title;  //extrae el titulo
            $date = $item->pubDate;  //extrae la fecha
            $guid = $item->guid;  //extrae el link de la imagen
            $description = strip_tags($item->description);  //extrae la descripcion
            if (strlen($description) > 200) { //limita la descripcion a 400 caracteres
                $stringCut = substr($description, 0, 200);                   
                $description = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';
            }
            if ($i < 25) { // extrae solo 16 items
                echo "
                    <div class='card mb-3'>
                        <div class='card-body'>
                            <h5 class='card-title'>{$title}</h5>
                            <p class='card-text'>{$description}</p>
                            <a href='{$link}' target='_blank' class='btn btn-secondary float-left'>Leer todo <i class='fas fa-chevron-right'></i></a>
                            <a href='hates.html?title=".urlencode($title)."&desc=".urlencode($description)."&url=".urlencode($link)."' class='btn btn-primary float-right'>Odiar <i class='fas fa-chevron-right'></i></a>
                        </div>
                    </div>
                ";
            }
            $i++;
        }
    }

?>
    <div class="container pt-3 mt-2">
        <div class="row">
            <div class="col-sm">
                <?php feed("https://feeds.bbci.co.uk/mundo/rss.xml"); ?>
            </div>
        </div>
    </div>
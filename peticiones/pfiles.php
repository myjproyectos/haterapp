<?php 
    header('Access-Control-Allow-Origin: *');
    
    require "../query.php";

    $email = $_POST['email'];
    
    $ahates = [];

    if($_POST['noticia']){
        $char = ["/", "-", "%2F"];
        $notiURL =  str_replace($char, "_", strstr($_POST['noticia'], "mundo"));
        $_n = infoNoti($notiURL);
        if($_n != []){
            $ahates = $_n->comments == NULL ? [] :json_decode($_n->comments);
        }
    }

    if($_POST['index'] || $_POST['index'] == 0){
        $index = $_POST['index'];  
    }
    if($_POST['reaccion']){
        $reaccion = $_POST['reaccion'];
    }

    switch ($_POST['tipo']) {
        case 'registro':
            register($email, $_POST['pass']);
            break;
        
        case 'articulo':
            $hate = $_POST['hate'];
            
            array_push($ahates, ['comment' => $hate, 'user' => $email, 'resp' => [], 'reaccion' => ['rA' => 0, 'rB' => 0, 'rC' => 0, 'rD' => 0]]);
            updateInfoNoti($ahates, $_n->id);
            updateJoin($_n->id, user($email)->id);

            break;

        case 'respuesta':      

            $resp = $_POST['resp'];
            
            $t1 = $ahates[$index]->resp;
            array_push($t1, ['res' => $resp, 'reaccion' => ['rA' => 0, 'rB' => 0, 'rC' => 0, 'rD' => 0]]);
            $ahates[$index]->resp = $t1;
            updateInfoNoti($ahates, $_n->id);
            
            break;

        case 'reaccion1':

            $t1 = $ahates[$index]->reaccion->{$reaccion} + 1;
            $ahates[$index]->reaccion->{$reaccion} = $t1;
            updateInfoNoti($ahates, $_n->id);

            break;

        case 'reaccion2':
            
            $it = explode("-", $index);
            $i1 = $it[0];
            $i2 = $it[1];

            $t1 = $ahates[$i1]->resp[$i2]->reaccion->{$reaccion} + 1;
            $ahates[$i1]->resp[$i2]->reaccion->{$reaccion} = $t1;
            updateInfoNoti($ahates, $_n->id);

            break;
    }

?>



<?php if(infoNoti($notiURL) != [] && infoNoti($notiURL)->comments != NULL){ ?>
    <ul class="list-group mb-5 pb-2">
        <?php 
        $array = json_decode(infoNoti($notiURL)->comments);
        foreach ($array as $key => $val) {
        ?>
            <li class="list-group-item pl-3 pr-3 position-relative padre">
                <div class="media position-relative">
                    <img width="30px" class="rounded-circle mr-2" src="img/usuario.svg" alt="Icon user">
                    <div class="media-body">
                        <strong><?php echo $val->comment; ?></strong>        
                    </div>
                    <span class="badge reaction" role="button" aria-label="<?php echo $key; ?>"><i class="far fa-meh-blank"></i></span>
                    <span class="badge reply" role="button" aria-label="<?php echo $key; ?>"><i class="fas fa-reply"></i></span>
                    <div class="position-absolute reactions reactionRight d-inline-flex" style="<?php if(count($val->resp) > 0){ echo "bottom : -13px"; }?>">
                        <?php if($val->reaccion->{"rA"} > 0) { ?>
                            <span class="badge d-flex" style="align-items: center"><img style="margin-right: 2px;" src="img/rock.svg" width="10px" alt="Así se habla"><?php echo $val->reaccion->{"rA"}; ?></span>
                        <?php }; if($val->reaccion->{"rB"} > 0) { ?>
                            <span class="badge d-flex" style="align-items: center"><img style="margin-right: 2px;" src="img/poop.svg" width="10px" alt="Me cagas"><?php echo $val->reaccion->{"rB"}; ?></span>
                        <?php }; if($val->reaccion->{"rC"} > 0) { ?>
                            <span class="badge d-flex" style="align-items: center"><img style="margin-right: 2px;" src="img/stupid.svg" width="10px" alt="Imbécil"><?php echo $val->reaccion->{"rC"}; ?></span>
                        <?php }; if($val->reaccion->{"rD"} > 0) { ?>
                            <span class="badge d-flex" style="align-items: center"><img style="margin-right: 2px;" src="img/jodase.svg" width="10px" alt="Jodase"><?php echo $val->reaccion->{"rD"}; ?></span>
                        <?php } ?>
                    </div>
                </div>
                <?php if(count($val->resp) > 0){ ?>
                <ul class="list-group  mt-3">
                    <?php foreach($val->resp as $k => $v) { ?>
                        <li class="list-group-item pb-3">
                            <div class="media">
                                <img width="30px" class="rounded-circle mr-2" src="img/usuario.svg" alt="Icon user">
                                <div class="media-body">
                                    <strong><?php echo $v->res; ?></strong>
                                </div>
                                <span class="badge reaction2" role="button" aria-label="<?php echo $k; ?>"><i class="far fa-meh-blank"></i></span>
                                <div class="position-absolute reactions d-inline-flex">
                                    <?php if($v->reaccion->{"rA"} > 0) { ?>
                                        <span class="badge d-flex" style="align-items: center"><img style="margin-right: 2px;" src="img/rock.svg" width="10px" alt="Así se habla"><?php echo $v->reaccion->{"rA"}; ?></span>
                                    <?php }; if($v->reaccion->{"rB"} > 0) { ?>
                                        <span class="badge d-flex" style="align-items: center"><img style="margin-right: 2px;" src="img/poop.svg" width="10px" alt="Me cagas"><?php echo $v->reaccion->{"rB"}; ?></span>
                                    <?php }; if($v->reaccion->{"rC"} > 0) { ?>
                                        <span class="badge d-flex" style="align-items: center"><img style="margin-right: 2px;" src="img/stupid.svg" width="10px" alt="Imbécil"><?php echo $v->reaccion->{"rC"}; ?></span>
                                    <?php }; if($v->reaccion->{"rD"} > 0) { ?>
                                        <span class="badge d-flex" style="align-items: center"><img style="margin-right: 2px;" src="img/jodase.svg" width="10px" alt="Jodase"><?php echo $v->reaccion->{"rD"}; ?></span>
                                    <?php } ?>
                                </div>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
                <?php } ?>
            </li>
        <?php } ?>
    </ul>
<?php } else { ?>
    <p>Nadie a dicho algo al respecto se el primero</p>
<?php } ?>
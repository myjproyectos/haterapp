<?php
    require_once 'con.php';
     
    function getDB(){
        $host = DB_SERVER;
        $dbname = DB_DATABASE;
        $username = DB_USERNAME;
        $password = DB_PASSWORD;
        try {
            $conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
            $conn->exec("set names utf8");
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $conn;

        } catch (PDOException $pe) {
            die("Could not connect to the database $dbname :" . $pe->getMessage());
        }
    }
?>
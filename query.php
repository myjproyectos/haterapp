<?php 
    require "connect.php";
    
    function register($email, $pass){
        try {
            $q = "SELECT id FROM haterapp_users WHERE usuario=:email";
            $count = consultas($q, ["email"], [$email], 'str', true) -> rowCount();
            if($count < 1) {
                $q = "INSERT INTO haterapp_users(usuario,password) VALUES (:email,:hash_pass)";
                $hash_pass= hash('sha256', $pass); //Password encryption
                consultas($q, ["email", "hash_pass"], [$email, $hash_pass], 'str');
            } else {
                $db = null;
                return false;
            }
        } catch (PDOException $th) {
            echo '{"error":{"text_register":'. $th->getMessage() .'}}';
        }
    }

    function user($email){
        try{
            $q = "SELECT * FROM haterapp_users WHERE usuario=:email";
            $res = consultas($q, ["email"], [$email], 'str', true);
            $d = $res-> fetch(PDO::FETCH_OBJ);
            return $d;
        } catch(PDOException $e) {
            echo '{"error":{"text_user":'. $e->getMessage() .'}}';
        }
    }

    function infoNoti($noti){
        try {
            $q = "SELECT * FROM haterapp_comments WHERE noticia=:noti";
            $p = ["noti"];  $v = [$noti];   $t = 'str';
            $res = consultas($q, $p, $v, $t, true);
            $c = $res -> rowCount();
            if($c < 1) {
                $q = "INSERT INTO haterapp_comments(noticia) VALUES (:noti)";
                consultas($q, $p, $v, $t);
                $d = [];
            }else{
                $d = $res-> fetch(PDO::FETCH_OBJ);
            }
            return $d;
        } catch (PDOException $th) {
            // echo '{"error": {"text_infoNoti": '.$th->getMessage().'}}';
            echo 'actualmente el servidor se encuentra ocupado intenta mas tarde : :\'( a';
        }
    }

    function updateInfoNoti($coment, $noti){
        try {
            $json = json_encode($coment, TRUE);
            $q = "UPDATE haterapp_comments SET comments = :t WHERE id = :noti";
            consultas($q, ["t","noti"], [$json, $noti], 'str');
        } catch (PDOException $th) {
            echo '{"error": {"text_updateInfoNoti": '.$th->getMessage().'}}';
        }
    }

    function updateJoin($Nid, $Uid){
        try {
            $q = "SELECT * FROM haterapp_join WHERE id_comments=:idc AND id_users=:idu";
            $res = consultas($q, ["idc", "idu"], [$Nid, $Uid], 'int', true);
            $c = $res -> rowCount();
            if($c < 1){
                $q = "INSERT INTO haterapp_join(id_comments, id_users) VALUES (:idc, :idu)";
                consultas($q, ["idc", "idu"], [$Nid, $Uid], 'int');
            }
        } catch (PDOException $th) {
            echo '{"error": {"text_updateJoin": '.$th->getMessage().'}}';
        }
    }

    function consultas($cons, $param, $var, $type, $r = false){ //query, array[parametros], array[variables], 'int' o 'str', boolean
        $db = getDB();
        $s = $db->prepare($cons);
        $p = $type == 'str' ? PDO::PARAM_STR : PDO::PARAM_INT;
        for ($i=0; $i < count($param); $i++) { 
            $s->bindParam($param[$i], $var[$i], $p);
        }
        $s->execute();
        $db = null;
        if($r){ return $s; }
    }
    
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Pragma" content="no-cache">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://bootswatch.com/4/lux/bootstrap.css" rel="stylesheet" crossorigin="anonymous">

    <title>HaterApp</title>
    <style>
        .position-absolute-center{
            position: absolute;
            left: 50%;
            top: 0;
            transform: translateX(-50%);
        }
        .reactions{
            bottom: 3px;
            right: 15px;
        }
    </style>
</head>
<body>
<?php require "nav.php"; ?>